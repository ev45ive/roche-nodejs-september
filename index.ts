import http from "http";
import path from "path";
import fs from "fs";

const logStream = fs.createWriteStream("log.txt");

const server = http.createServer((req, res) => {
  console.log(req.headers);
  req.pipe(process.stdout);
  res.setHeader("Content-Type", "text/html");

  const filePath = path.join(__dirname, "public", "index.html");

  const fileStream = fs.createReadStream(filePath, {});

  logStream.write(`${new Date()} Loglog \r\n`);

  fileStream
    .on("error", err => {
      res.statusCode = 404;
      res.write(`<p>${err.message}</p>`);
      res.end();
    })
    .pipe(res);
});

server.listen(8080, () => {
  console.log("Listening on port 8080");
});
