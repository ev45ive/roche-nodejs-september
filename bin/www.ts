#!/usr/bin/env node

import { app } from "../app";
import { promisify } from "util";

const PORT = 8080;

import { client } from "../services/mongo";

(async () => {
  try{
    await promisify(client.connect.bind(client))();
    
    app.listen(PORT, () => {
      console.log(`Server listening on ${PORT}`);
    });

  }catch(e){
    console.error(e)
  }

})();
