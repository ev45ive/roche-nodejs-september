import express, { RequestHandler } from "express";
import path from "path";
import { profiles } from "./routes/profiles";
import { users } from "./routes/users";
import cookieParser from "cookie-parser";
import session from "express-session";
import morgan from "morgan";
import errorhandler from "errorhandler";
import passport from "passport";
import "./services/auth";
import './services/mongoose'
import dotenv from "dotenv";
import { api } from './routes/index';
dotenv.config({});
// process.env <- here

console.log(process.env.MYSQL_PASS)

export const app = express();

app.set("views", path.resolve(__dirname, "views"));
app.set("view engine", "ejs");

// Middleware
app.use("/public", express.static("./public"));
app.use("/uploads", express.static("./uploads"));

app.use(express.urlencoded({}));
app.use(express.json({}));
app.use(morgan("common", {}));
// app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))

app.use(
  session({
    name: "SESSION-SECRET-COOKIE",
    secret: process.env.SECRET!,
    resave: true,
    saveUninitialized: true,
    cookie: { /* secure: true, */ maxAge: 60000 }
  })
);
app.use(passport.initialize());
app.use(passport.session());

app.use(cookieParser("keyboard cat", {}));

app.use((req, res, next) => {
  res.locals.title = "Blog App";
  // next(new Error("Upss.. error"))
  next();
});


// Routers
app.use('/api',api)

app.use("/profiles", profiles);
app.use("/users", users);

// other routes:

app.get("/home", (req, res) => {
  if (req.session!.viewsCount) {
    req.session!.viewsCount++;
  } else {
    req.session!.viewsCount = 1;
  }

  res.render("home", {
    title: "Hey",
    viewsCount: req.session!.viewsCount || 0,
    message: req.cookies["message"] || ""
  });
});

app.get("/", (req, res) => {
  res.redirect(/* 302, */ "/about");
});

app.get("/about", (req, res) => {
  res.render("about");
});

const loggedIn:RequestHandler = (req, res, next) => {
  if (req.user) {
      next();
  } else {
      res.redirect('/users/login');
  }
} 

app.get("/admin", loggedIn, (req, res) => {
  res.render("about");
});

// Default error handler

// app.use((err: any, req: any, res: any, next: any) => {
//   console.error(err.stack);
//   res.status(500).send("Something broke!");
// });

if (process.env.NODE_ENV === "development") {
  // only use in development
  app.use(errorhandler());
}
