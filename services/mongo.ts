import { MongoClient } from "mongodb";

const uri =
  "mongodb+srv://test:test@rochetrainingmongocluster-z9hg6.mongodb.net/test";

export const client = new MongoClient(uri, { useNewUrlParser: true });


// mongo mongodb+srv://test:test@rochetrainingmongocluster-z9hg6.mongodb.net/test