import { IStrategyOptionsWithRequest, VerifyFunction } from "passport-local";
import https from "https";

import passport from "passport";
var LocalStrategy = require("passport-local").Strategy;
var OAuth2Strategy = require("passport-oauth").OAuth2Strategy;

// http://localhost:8080/users/login/spotify

/* Oauth */
passport.use(
  "provider",
  new OAuth2Strategy(
    {
      authorizationURL: "https://accounts.spotify.com/authorize",
      tokenURL: "https://accounts.spotify.com/api/token",
      clientID: "70599ee5812a4a16abd861625a38f5a6",
      clientSecret: "624d68ed7e3d4db685957481bf8a2b1c",
      callbackURL: "http://localhost:8080/users/provider/callback"
    },
    function(accessToken: any, refreshToken: any, profile: any, done: any) {
      // debugger
      done("placki");

      https.request(
        "https://api.spotify.com/v1/me",
        {
          headers: {
            Authorization: `Bearer ${accessToken}`
          }
        },
        res => {
          debugger;
        }
      );
    }
  )
);

/* Local: */

const verify: VerifyFunction = (username: string, password: string, done) => {
  // Problem geting authorization? - return error
  // if (err) { return done(err); }

  if (username !== "admin") {
    return done(null, false, { message: "Incorrect username." });
  }

  if (password !== "admin") {
    return done(null, false, { message: "Incorrect password." });
  }

  // Success!
  const user = { username: "admin" };
  return done(null, user);
};

const local = new LocalStrategy(
  {
    usernameField: "username",
    passwordField: "password"
  } as IStrategyOptionsWithRequest,
  verify
);

passport.use("local", local);

export default passport;

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});
