import mongoose from "mongoose";

mongoose.connect(
  "mongodb+srv://test:test@rochetrainingmongocluster-z9hg6.mongodb.net/test",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);

mongoose.connection.on("open", () => {
  console.log("mongoose connected");
});

