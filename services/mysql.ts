import mysql from "mysql";

var connection = mysql.createConnection({
  host: "roche-node-training.cdzvpmsfnuig.eu-west-1.rds.amazonaws.com",
  user: "admin",
  password: process.env.MYSQL_PASS,
  database: "node_training",
  insecureAuth: true,
});

connection.connect(err => {
  console.log(err);
  // connection.query(
  //   "SELECT 1 + 1 AS solution",
  //   (err: any, rows: any, fields: any) => {
  //     if (err) throw err;

  //     console.log("The solution is: ", rows[0].solution);
  //   }
  // );
});
