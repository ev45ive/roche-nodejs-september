import { Router } from "express";
import { Post } from "../../models/post";
import { User } from "../../models/user";

// /api/posts
export const postsApi = Router();

postsApi.get("/", async (req, res) => {
  const result = await Post.find({}).limit(10);
  res.send(result);
});

postsApi.post("/", async (req, res) => {
  const post = new Post({
    title: "Hello!",
    author: new User({
      username: "new User"
    })
  });

  await post.save({});

  res.status(201).send(post);
});

postsApi.param("id", async (req, res, next) => {
  res.locals.post = await Post.findById(req.params.id);
  if (!res.locals.post) {
    return res.status(404).send();
  }
  next();
});

postsApi.put("/:id", async (req, res) => {
  await res.locals.post.update({
    ...req.body
  });

  res.status(202).send(res.locals.post);
});

postsApi.delete("/:id", async (req, res) => {
  await res.locals.post.remove();

  res.send(res.locals.post);
});
