import { Route, Controller, Get } from "tsoa";

@Route()
export class StuffController extends Controller {
  
  @Get() async getAll() {
    return [1, 2, 3];
  }
}
