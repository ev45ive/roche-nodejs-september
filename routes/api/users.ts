import { Router } from "express";
import { client } from "../../services/mongo";
import { ObjectId } from "mongodb";

export const usersApi = Router();

usersApi.get("/", async (req, res) => {
  const users = client.db("test").collection("users");

  const cursor = users
    .find({
      // username: { $regex: req.query}

      ...(req.query.search && { $text: { $search: req.query.search } })
    })
    .project({ password: 0 });

  const result = await cursor
    .limit(10)
    .sort({ username: 1 })
    .toArray();

  res.json(result);
});

type UserPayload = {
  username: string;
  password: string;
  email: string;
};

usersApi.post("/", async (req, res) => {
  const users = client.db("test").collection("users");

  const user: UserPayload = req.body;
  const result = await users.insertOne(user);

  res.json(result.insertedId);
});

usersApi.put("/:userId", async (req, res) => {
  const users = client.db("test").collection("users");

  const user: UserPayload = req.body;

  const result = await users.updateOne(
    {
      _id: new ObjectId(req.params.userId)
    },
    {
      $set: {
        ...user,
        active: true
      },
      $currentDate: {
        lastModified: true
      }
    }
    // {
    //   upsert:true
    // }
  );
  res.json(result);
});

usersApi.delete("/:userId", (req, res) => {});

// Streams and cursors approach:
// for await(let user of cursor){
//   console.log(user)
// }
// https://www.npmjs.com/package/JSONStream

/* 
fetch('http://localhost:8080/api/users',{
	method:'POST', 
	headers:{ 
		'Content-Type': 'application/json'
 	}, 
	body: JSON.stringify({
		username:'user', 
		password:'user', 
		email:'user@user'
    })
})

*/
