import express from "express";
import { findMany, findAll, findOne } from "../models/profiles";

export const profiles = express.Router();

// Router middleware
profiles.use((req, res, next) => {
  res.locals.title = "Profiles";
  next();
})

profiles.get("/me", (req, res) => {
  res.send("works!");
});

profiles.get("/", (req, res) => {
  const filter = req.query["filter"];

  res.locals.profiles = filter //
    ? findMany({ username: filter })
    : findAll();

  res.render("profiles");
});

profiles.get("/:username", (req, res) => {
  const { username } = req.params;

  res.locals.profile = findOne({ username });
  // res.locals.title = username + ' Blog'

  res.render("profile");
});

profiles.get("/:username/posts", (req, res) => {});
