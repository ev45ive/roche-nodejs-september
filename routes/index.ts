import { Router } from "express";
import { usersApi } from "./api/users";
import { postsApi } from "./api/posts";

// /api/* Routes
export const api = Router();

api.use("/users", usersApi);
api.use("/posts", postsApi);

// api.use("/posts", postsApi);
// api.use("/comments", commentsApi);
