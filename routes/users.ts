import express from "express";

export const users = express.Router();

// register
users.get("/register", (req, res) => {
  res.render("register", {
    username: "",
    email: "",
    password: "",
    avatar: "",
    errors: []
  });
});

import multer from "multer";
import passport from "passport";
const uploader = multer({ dest: "uploads/" /*  fileFilter(){} */ });
const cpUpload = uploader.fields([
  { name: "avatar", maxCount: 1 },
  { name: "gallery", maxCount: 8 }
]);

// handle form submission
users.post("/register", cpUpload, (req, res) => {
  const { username, email, password } = req.body;

  const avatar = "/" + (req.files as any)["avatar"][0].path;

  const errors = [];
  if (!username) {
    errors.push("Username required");
  }

  if (!errors.length) {
    res.cookie("message", "Success!", {
      maxAge: 30 * 1000
    });
    res.redirect("/home");
    return;
  }

  res.render("register", {
    username,
    email,
    password,
    avatar,
    errors
  });
});

// login
users.get("/login", (req, res) => {
  res.render("login");
});

users.post(
  "/login",
  passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/users/login",
    // failureFlash:'Flash',
    failureMessage: "Messsage"
  })
  /* , (req,res)=>{
    // res.redirect('')
  } */
);

// users.post('/login/facebook', passport.authenticate("facebook") )

/* placki@placki.com : ******  */

users.get(
  "/login/spotify",
  passport.authenticate("provider", {
    scope: ["user-read-private", "user-read-email"]
  })
);

users.get("/provider/callback", passport.authenticate("provider"));


// https://github.com/auth0/express-jwt
users.post("/jwt-login", (req, res) => {
  // const token = sign( req.body.username )

  // res.json({
  //   accessToken: token
  // });
});

users.get('top_secret',(req,res)=>{
  // const user_provfile =  decrypt( req.headers['authorization'] )
})
