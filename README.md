git clone https://bitbucket.org/ev45ive/roche-nodejs-september


https://tinyurl.com/node-roche


cd roche-nodejs-september
npm i
npm start

---

npm i -s express
npm i -D @types/express

## Recompile on changes
tsc
tsc --watch

## Restart on changes
npm i -g nodemon 
nodemon ./dist/index.js

## Ts dev tools
ts-node
ts-node-dev --respawn ./index.ts

## Debugging
debugger;

node --inspect ./dist/index.js
node --inspect-brk ./dist/index.js

ts-node-dev --respawn --inspect-brk -- ./index.ts
ts-node-dev --respawn --inspect -- ./index.ts

ts-node-dev --respawn --inspect-brk -- ./index.ts

chrome://inspect/#devices

## Templates
npm install ejs --save

## File uploads
npm i multer


