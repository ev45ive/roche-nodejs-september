import { Schema, model } from "mongoose";

export const userSchema = new Schema({
  username: String,
  password: {
    type: String,
    select: false
  },
  email: String
});

export const User = model("User", userSchema);
