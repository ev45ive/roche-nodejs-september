import mongoose, {
  Schema,
  Model,
  model,
  Document,
  MongooseDocument
} from "mongoose";
import { userSchema } from "./user";

export const commentSchema = new Schema({
  title: String,
  body: String,
  likes: Number,
  author: userSchema
});

interface Post {
  title: string;
  body: string;
  likes: number;
}

export const postSchema = new Schema<Post>({
  title: {
    type: String,
    index: "text"
  },
  body: String,
  likes: Number,
  author: userSchema,
  comments: [commentSchema],
  meta: {
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    published: Boolean
  }
});
postSchema.index({ title: "text", body: "text" });

class PostModel implements Post {
  title = "";
  body = "";
  likes = 0;

  documentMethod() {}
  static modelMethod() {}
}

postSchema.loadClass(PostModel);

type PostDocumentApi = PostModel & Document;
type PostModelApi = typeof PostModel & Model<PostModel & Document>;

export const Post = model<PostDocumentApi, PostModelApi>("Post", postSchema);

// Post.modelMethod;

// const p = new Post();
// p.title;
// p.documentMethod();
// p.save();
