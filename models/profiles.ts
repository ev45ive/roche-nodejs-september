const profiles = [{ name: "Alice" }, { name: "Bob" }];

export const findAll = () => {
  return profiles;
};

export const findOneById = (id: number) => {};

export const findMany = (query: { username: string }) => {
  return profiles.filter(
    // match by username
    p => p.name.includes(query.username)
  );
};

export const findOne = (query: { username: string }) => {
  return profiles.find(
    // match by username
    p => p.name.includes(query.username)
  );
};
